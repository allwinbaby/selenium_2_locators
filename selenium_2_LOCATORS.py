from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get("https://www.geeksforgeeks.org/locator-strategies-selenium-python/")
NavBar_ID = driver.find_element(By.ID, "nav-tab-main")
print(NavBar_ID.get_attribute("style"))
suggestion_section_NAME = driver.find_element(By.NAME, "suggestion-section")
print(suggestion_section_NAME)
Ankor = driver.find_element(By.LINK_TEXT, 'Skip to content')
print(Ankor.get_attribute("href"))
class_Name_elements = driver.find_elements(By.CLASS_NAME, "header-main__container")
print(len(class_Name_elements))
by_tag_elements = driver.find_elements(By.TAG_NAME, "a")
print(len(by_tag_elements))
by_css_selector_element = driver.find_element(By.CSS_SELECTOR,"div#nav-tab-main")
print(by_css_selector_element)
by_XPATH = driver.find_element(By.XPATH,"//div[@class = '_logo']//img[contains(@alt,'geeksforgeeks')]")
print(by_XPATH.get_attribute("src"))
driver.close()

# OUTPUT --Screenshot of output added in README.MD file


